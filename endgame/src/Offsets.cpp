#include "../Include/Offsets.h"

namespace hazedumper
{
	namespace netvars
	{
		int m_ArmorValue = 0x0;
		int m_aimPunchAngle = 0x0;
		int m_aimPunchAngleVel = 0x0;
		int m_bBombPlanted = 0x0;
		int m_bHasDefuser = 0x0;
		int m_bHasHelmet = 0x0;
		int m_bIsDefusing = 0x0;
		int m_bIsScoped = 0x0;
		int m_bSpotted = 0x0;
		int m_bSpottedByMask = 0x0;
		int m_dwBoneMatrix = 0x0;
		int m_fFlags = 0x0;
		int m_flC4Blow = 0x0;
		int m_flDefuseCountDown = 0x0;
		int m_flDefuseLength = 0x0;
		int m_hActiveWeapon = 0x0;
		int m_iCrosshairId = 0x0;
		int m_iFOV = 0x0;
		int m_iHealth = 0x0;
		int m_iItemDefinitionIndex = 0x0;
		int m_iItemIDHigh = 0x0;
		int m_iShotsFired = 0x0;
		int m_iState = 0x0;
		int m_iTeamNum = 0x0;
		int m_lifeState = 0x0;
		int m_vecOrigin = 0x0;
		int m_vecViewOffset = 0x0;
	}

	namespace signatures
	{
		int dwClientState = 0x0;
		int dwClientState_MaxPlayer = 0x0;
		int dwClientState_PlayerInfo = 0x0;
		int dwClientState_ViewAngles = 0x0;
		int dwEntityList = 0x0;
		int dwGameRulesProxy = 0x0;
		int dwLocalPlayer = 0x0;
		int dwRadarBase = 0x0;
		int dwViewMatrix = 0x0;
		int dwWeaponTableIndex = 0x0;
		int m_bDormant = 0x0;
	}
}