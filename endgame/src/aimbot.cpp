#include <iostream>
#include <Windows.h>
#include "../Include/aimbot.h"
#include "../Include/MemoryHandling.h"
#include "../Include/Offsets.h"

struct Vector
{
	float x, y, z;
};

struct Player
{
	Vector position;
	Vector viewAngles;
	DWORD  clientState;
	int    team;
	float  viewMatrix[4][4];

	void ReadMemory()
	{
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwLocalPlayer), &endgame::offsets::localPlayer, sizeof(DWORD), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::localPlayer + hazedumper::netvars::m_vecOrigin), &position, sizeof(Vector), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::localPlayer + hazedumper::netvars::m_iTeamNum), &team, sizeof(int), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::engineModule + hazedumper::signatures::dwClientState), &clientState, sizeof(DWORD), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(clientState + hazedumper::signatures::dwClientState_ViewAngles), &viewAngles, sizeof(Vector), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwViewMatrix), &viewMatrix, sizeof(viewMatrix), 0);
	}

}playerData;

struct Entity
{
	DWORD entityBase;
	DWORD boneMatrix;
	float bonePosition[3];
	int isDormant;
	int lifeState;
	int team;
	int health;
	int spotted;

	void ReadMemory(int index, int bone)
	{
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwEntityList + (index * 0x10)), &entityBase, sizeof(DWORD), NULL);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(entityBase + hazedumper::signatures::m_bDormant), &isDormant, sizeof(int), NULL);

		if (isDormant == 0)
		{
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(entityBase + hazedumper::netvars::m_dwBoneMatrix), &boneMatrix, sizeof(DWORD), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(boneMatrix + 0x30 * bone + 0x0C), &bonePosition[0], sizeof(bonePosition), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(boneMatrix + 0x30 * bone + 0x1C), &bonePosition[1], sizeof(bonePosition), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(boneMatrix + 0x30 * bone + 0x2C), &bonePosition[2], sizeof(bonePosition), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(entityBase + hazedumper::netvars::m_lifeState), &lifeState, sizeof(int), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(entityBase + hazedumper::netvars::m_iTeamNum), &team, sizeof(int), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(entityBase + hazedumper::netvars::m_iHealth), &health, sizeof(int), 0);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(entityBase + hazedumper::netvars::m_bSpottedByMask), &spotted, sizeof(int), NULL);
		}
	}

}entityData[32];

__forceinline float Get3D(float X, float Y, float Z, float eX, float eY, float eZ)
{
	return(sqrtf((eX - X) * (eX - X) + (eY - Y) * (eY - Y) + (eZ - Z) * (eZ - Z)));
}

float CloseEnt()
{
	//Variables
	float lowest = 100000.0f;
	int   index  = 0;
	float temp;

	for (unsigned i = 0; i < 32; i++)
	{
		//Store Distances In Array
		temp = Get3D(playerData.position.x, playerData.position.y, playerData.position.z, entityData[i].bonePosition[0], entityData[i].bonePosition[1], entityData[i].bonePosition[2]);

		//If Enemy Has Lower Distance The Player 1, Replace (var)Lowest With Current Enemy Distance
		if (temp < lowest && entityData[i].health > 0 && entityData[i].team != playerData.team && entityData[i].spotted > 0)
		{
			lowest = temp;
			index  = i;
		}
	}
	
	return index;
}

Vector CalculateAngle(Vector src, float dir[3])
{
	Vector angle;
	Vector newAngle;

	angle.x = dir[0] - src.x;
	angle.y = dir[1] - src.y;
	angle.z = dir[2] - src.z;

	float Magnitude = sqrt(angle.x * angle.x + angle.y * angle.y + angle.z * angle.z);
	newAngle.x      = -atan2(angle.z - 64.0f, Magnitude) * 180 / 3.14;
	newAngle.y      = atan2(angle.y, angle.x) * 180 / 3.14;
	newAngle.z      = 0;
	
	return newAngle;
}

void endgame::aimbot::RecoilControl()
{
	std::cout << "RCS Started..." << std::endl;

	float punchAngle[]          = { 0.0f, 0.0f };
	float oldPunchAngle[]       = { 0.0f, 0.0f };
	const float pixPerDegree    = width / 90;
	
	float middleX = tSize.left + ((tSize.right - tSize.left) / 2.0f), middleY = tSize.top + ((tSize.bottom - tSize.top) / 2.0f);

	for (;;)
	{

		if (GetAsyncKeyState(VK_LBUTTON))
		{
			playerData.ReadMemory();
			ReadProcessMemory(endgame::offsets::hProc, (float*)(endgame::offsets::localPlayer + hazedumper::netvars::m_aimPunchAngle), &punchAngle, sizeof(punchAngle), NULL);

			for (auto& i : punchAngle)
				if (i == 0.0f)
					continue;

			for (unsigned i = 0; i < 2; ++i)
				punchAngle[i] -= oldPunchAngle[i];
			
			if (punchAngle[0] == 0.0f && punchAngle[1] == 0.0f)
				continue;
			else
			{
				SetCursorPos(middleX, middleY + (punchAngle[0] * pixPerDegree * 2.0f * -1.0f));

				Sleep(5);

				SetCursorPos(middleX + (punchAngle[1] * pixPerDegree * 2.0f), middleY);

				Sleep(20);
			}

			for (unsigned i = 0; i < 2; ++i)
				oldPunchAngle[i] += punchAngle[i];
		}
		else
		{
			punchAngle[0]    = 0.0f;
			punchAngle[1]    = 0.0f;
			oldPunchAngle[0] = 0.0f;
			oldPunchAngle[1] = 0.0f;
		}

		Sleep(5);
	}
}

void endgame::aimbot::Run()
{
	std::cout << "Aimbot running" << std::endl;

	for (;;)
	{
		if (GetAsyncKeyState(0x58))
		{
			playerData.ReadMemory();

			for (unsigned i = 0; i < 32; ++i)
				entityData[i].ReadMemory(i + 1, 8);
			
			int entityNum = CloseEnt();
			Vector angle2Aim = CalculateAngle(playerData.position, entityData[entityNum].bonePosition);
			
			int diffX = abs(angle2Aim.x - playerData.viewAngles.x);
			int diffY = abs(angle2Aim.y - playerData.viewAngles.y);

			if (entityData[entityNum].lifeState == 0)
			{
				if (diffX < 10 && diffY < 10)
				{
					float angle2AimXY[2];
					endgame::math::WorldToScreen(entityData[entityNum].bonePosition, angle2AimXY, playerData.viewMatrix);

					if (abs(angle2AimXY[0] - (tSize.right - (width / 2))) > 1.0f || abs(angle2AimXY[1] - (tSize.bottom - (height / 2))) > 1.5f)
					{
						if (abs(angle2AimXY[0] - (tSize.right - (width / 2))) > 1.0f && angle2AimXY[0] - (tSize.right - (width / 2)) < 0)
						{
							SetCursorPos((tSize.right - (width / 2)) - (abs(angle2AimXY[0] - (tSize.right - (width / 2))) / 4), (tSize.bottom - (height / 2)));
						}
						else
						{
							SetCursorPos((tSize.right - (width / 2)) + (abs(angle2AimXY[0] - (tSize.right - (width / 2))) / 4), (tSize.bottom - (height / 2)));
						}

						Sleep(5);

						if (abs(angle2AimXY[1] - (tSize.bottom - (height / 2))) > 1.5f && angle2AimXY[1] - (tSize.bottom - (height / 2)) < 0)
						{
							SetCursorPos((tSize.right - (width / 2)), (tSize.bottom - (height / 2)) - (abs(angle2AimXY[1] - (tSize.bottom - (height / 2))) / 4));
						}
						else
						{
							SetCursorPos((tSize.right - (width / 2)), (tSize.bottom - (height / 2)) + (abs(angle2AimXY[1] - (tSize.bottom - (height / 2))) / 4));
						}
					}
				}
			}
		}
		
		Sleep(1);
	}
}