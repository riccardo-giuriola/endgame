#include "../Include/hEsp.h"

const int NumOfPlayers = 32;
int blowTimer = 45000;
int isPlanted = 0;
int radarSize = 0;
int boxX = 0;
int boxY = 0;

typedef struct
{
	float flMatrix[4][4];
}WorldToScreenMatrix_t;

struct MyPlayer_t
{
	int Team;
	int Health;
	float Position[3];
	float viewAngles[3];
	DWORD clientState;
	WorldToScreenMatrix_t WorldToScreenMatrix;

	void ReadInformation()
	{
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwLocalPlayer), &endgame::offsets::localPlayer, sizeof(DWORD), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::localPlayer + hazedumper::netvars::m_iTeamNum), &Team, sizeof(int), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::localPlayer + hazedumper::netvars::m_iHealth), &Health, sizeof(int), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::engineModule + hazedumper::signatures::dwClientState), &clientState, sizeof(DWORD), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(clientState + hazedumper::signatures::dwClientState_ViewAngles), &viewAngles, sizeof(float[3]), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::localPlayer + hazedumper::netvars::m_vecOrigin), &Position, sizeof(float[3]), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwViewMatrix), &WorldToScreenMatrix, sizeof(WorldToScreenMatrix), 0);
	}
}MyPlayer;

struct PlayerList_t
{
	DWORD BaseEntity;
	DWORD boneMatrix;
	DWORD RadarBase;
	DWORD RadarPtr;
	int ActiveWeapon;
	int WeaponID;

	float head[3];

	int isDormant;

	int Team;
	int Health;
	int Armor;
	int lifeState;
	int flag;
	int myWeaponID;
	float Position[3];
	char Name[32];

	void ReadInformation(int Player)
	{
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwEntityList + (Player * 0x10)), &BaseEntity, sizeof(DWORD), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(BaseEntity + hazedumper::signatures::m_bDormant), &isDormant, sizeof(int), 0);
		
		if (isDormant == 0)
		{
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwRadarBase), &RadarBase, sizeof(DWORD), 0);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(RadarBase + 0x78), &RadarPtr, sizeof(DWORD), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(RadarPtr + (0x174 * (Player + 2)) + 0x18), &Name, sizeof(Name), NULL);

			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(BaseEntity + hazedumper::netvars::m_iTeamNum), &Team, sizeof(int), 0);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(BaseEntity + hazedumper::netvars::m_iHealth), &Health, sizeof(int), 0);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(BaseEntity + hazedumper::netvars::m_ArmorValue), &Armor, sizeof(int), 0);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(BaseEntity + hazedumper::netvars::m_vecOrigin), &Position, sizeof(float[3]), 0);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(BaseEntity + hazedumper::netvars::m_lifeState), &lifeState, sizeof(int), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(BaseEntity + hazedumper::netvars::m_fFlags), &flag, sizeof(int), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(BaseEntity + hazedumper::netvars::m_dwBoneMatrix), &boneMatrix, sizeof(DWORD), NULL);

			//Entity weapon
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(BaseEntity + hazedumper::netvars::m_hActiveWeapon), &ActiveWeapon, sizeof(int), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwEntityList + ((ActiveWeapon & 0xFFF) - 1) * 0x10), &WeaponID, sizeof(int), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(WeaponID + hazedumper::netvars::m_iItemDefinitionIndex), &myWeaponID, sizeof(int), NULL);
			//std::cout << myWeaponID << std::endl;

			//head
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(boneMatrix + 0x30 * 10 + 0x0C), &head[0], sizeof(float), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(boneMatrix + 0x30 * 10 + 0x1C), &head[1], sizeof(float), NULL);
			ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(boneMatrix + 0x30 * 10 + 0x2C), &head[2], sizeof(float), NULL);
		}
	}
}PlayerList[NumOfPlayers];

LPCSTR selectWeapon(int weaponID)
{
	switch (weaponID)
	{
		case 1: return "Desert Eagle";

		case 2: return "Dual Berettas";

		case 3: return "Five-SeveN";

		case 4: return "Glock-18";

		case 7: return "AK-47";

		case 8: return "AUG";

		case 9: return "AWP";

		case 10: return "FAMAS";

		case 11: return "G3SG1";

		case 13: return "Galil AR";

		case 14: return "M249";

		case 16: return "M4A4";

		case 17: return "MAC-10";

		case 19: return "P90";

		case 23: return "MP5-SD";

		case 24: return "UMP-45";

		case 25: return "XM1014";

		case 26: return "PP-Bizon";

		case 27: return "MAG-7";

		case 28: return "Negev";

		case 29: return "Sawed-Off";

		case 30: return "Tec-9";

		case 31: return "Zeus x27";

		case 32: return "P2000";

		case 33: return "MP7";

		case 34: return "MP9";

		case 35: return "Nova";

		case 36: return "P250";

		case 37: return "Ballistic Shield";

		case 38: return "SCAR-20";

		case 39: return "SG 553";

		case 40: return "SSG 08";

		case 41: return "Knife";

		case 42: return "Knife";

		case 43: return "Flashbang";

		case 44: return "High Explosive Grenade";

		case 45: return "Smoke Grenade";

		case 46: return "Molotov";

		case 47: return "Decoy Grenade";

		case 48: return "Incendiary Grenade";

		case 49: return "C4 Explosive";

		case 59: return "Knife";

		case 60: return "M4A1-S";

		case 61: return "USP-S";

		case 63: return "CZ75-Auto";

		case 64: return "R8 Revolver";

		case 69: return "Bare Hands";

		case 81: return "Fire Bomb";

		case 82: return "Diversion Device";

		case 83: return "Frag Grenade";

		default: return "Knife";
	}
}

void DrawRadar(int i)
{
	int playerDim[2] = {8, 8};
	int radarScale = 6 * ((tSize.right - tSize.left) / 1000);
	int r = 0;
	int g = 0;
	int b = 0;

	if (tSize.right && boxX == 0 && boxY == 0)
	{
		radarSize = 0.15 * (tSize.right - tSize.left);
		boxX = 10;
		boxY = ((tSize.bottom - tSize.top) / 2) - 50;
	}

	float angle = MyPlayer.viewAngles[1] * (3.14f / 180.0f) + 80;

	double Cos = cos(angle);
	double Sin = sin(angle);

	int Xlmao = (width / 2 + ((PlayerList[i - 1].Position[0] - MyPlayer.Position[0]) / radarScale));
	int Ylmao = (height / 2 - ((PlayerList[i - 1].Position[1] - MyPlayer.Position[1]) / radarScale));

	int dx = Xlmao - width / 2;
	int dy = Ylmao - height / 2;

	double x = Cos * dx - Sin * dy + boxX + (radarSize / 2);
	double y = Sin * dx + Cos * dy + boxY + (radarSize / 2);
	
	if (PlayerList[i - 1].Team == MyPlayer.Team)
	{
		r = 255;
		g = 213;
		b = 0;
	}
	else
	{
		r = 255;
		g = 0;
		b = 0;
	}

	POINT p;
	if (GetCursorPos(&p))
	{
		int gamePaused;
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::engineModule + 0x8AE894), &gamePaused, sizeof(int), NULL);
		if (gamePaused == 1)
		{
			if ((p.x - tSize.left) > boxX && (p.x - tSize.left) < (boxX + radarSize) && (p.y - tSize.top) > boxY && (p.y - tSize.top) < (boxY + radarSize))
			{
				DrawCursorLine(p.x - tSize.left, p.y - tSize.top, (p.x - tSize.left) + 6, (p.y - tSize.top) + 20, 0, 255, 0, 255);
				DrawCursorLine(p.x - tSize.left, p.y - tSize.top, (p.x - tSize.left) + 20, (p.y - tSize.top) + 6, 0, 255, 0, 255);

				if (GetAsyncKeyState(VK_LBUTTON))
				{
					DrawCursorLine(p.x - tSize.left, p.y - tSize.top, (p.x - tSize.left) + 6, (p.y - tSize.top) + 20, 0, 255, 0, 255);
					DrawCursorLine(p.x - tSize.left, p.y - tSize.top, (p.x - tSize.left) + 20, (p.y - tSize.top) + 6, 0, 255, 0, 255);
					boxX = (p.x - tSize.left) - (radarSize / 2);
					boxY = (p.y - tSize.top) - (radarSize / 2);
				}
			}
			else
			{
				DrawCursorLine(p.x - tSize.left, p.y - tSize.top, (p.x - tSize.left) + 6, (p.y - tSize.top) + 20, 255, 255, 255, 255);
				DrawCursorLine(p.x - tSize.left, p.y - tSize.top, (p.x - tSize.left) + 20, (p.y - tSize.top) + 6, 255, 255, 255, 255);
			}
		}
	}

	//fuck off epileptics mode
	int boxR = rand() % 255 + 1;
	int boxG = rand() % 255 + 1;
	int boxB = rand() % 255 + 1;

	DrawBox(boxX, boxY, radarSize, radarSize, NULL, boxR, boxG, boxB, 255, 4);
	//DrawLine(tSize.left + 350, tSize.top + 250, tSize.left + 650, tSize.top + 250, 20, 20, 20, 255, 2);
	//DrawLine(tSize.left + 500, tSize.top + 100, tSize.left + 500, tSize.top + 400, 20, 20, 20, 255, 2);
	FillRGB((boxX + (radarSize / 2)) - (playerDim[0] / 2), (boxY + (radarSize / 2)) - (playerDim[1] / 2), playerDim[0], playerDim[1], 0, 255, 0, 255);

	if (x > boxX && x < (boxX + radarSize) && y > boxY && y < (boxY + radarSize))
	{
		FillRGB((int)x, (int)y, playerDim[0], playerDim[1], r, g, b, 255);
	}
}

void DrawESP()
{
	MyPlayer.ReadInformation();

	for (int i = 1; i <= NumOfPlayers; i++)
	{
		PlayerList[i - 1].ReadInformation(i);

		if (PlayerList[i - 1].Health > 0 && PlayerList[i - 1].lifeState == 0 && PlayerList[i - 1].isDormant == 0)
		{
			float EnemyXY[2];
			float HeadXY[2];

			DrawRadar(i);

			if (endgame::math::WorldToScreen(PlayerList[i - 1].head, HeadXY, MyPlayer.WorldToScreenMatrix.flMatrix) && endgame::math::WorldToScreen(PlayerList[i - 1].Position, EnemyXY, MyPlayer.WorldToScreenMatrix.flMatrix))
			{
				float distance = endgame::math::Get3dDistance(MyPlayer.Position, PlayerList[i - 1].Position);
				float height = (HeadXY[1] - (10000 / distance)) - EnemyXY[1];
				float width = height / 2.6f;
				int r = 0;
				int g = 0;
				int b = 0;

				if (PlayerList[i - 1].Team != MyPlayer.Team)
				{
					r = 0;
					g = 0;
					b = 255;
				}
				else
				{
					r = 0;
					g = 255;
					b = 0;
				}

				//Player Box
				DrawBox(EnemyXY[0] - tSize.left - width / 2, EnemyXY[1] - tSize.top, width, height, NULL, r, g, b, 255);

				//Player health bar
				FillRGB((EnemyXY[0] - tSize.left + width / 2) - 600 / distance, EnemyXY[1] - tSize.top, 500 / distance, (height / 100) * PlayerList[i - 1].Health, 155, 0, 0, 255);

				if (PlayerList[i - 1].Armor > 0)
				{
					//Player armor bar
					FillRGB((EnemyXY[0] - tSize.left - width / 2) + 100 / distance, EnemyXY[1] - tSize.top, 500 / distance, (height / 100) * PlayerList[i - 1].Armor, 0, 0, 155, 255);
				}

				LPCSTR weaponName = selectWeapon(PlayerList[i - 1].myWeaponID);
				if (weaponName != "")
				{
					//Draw weapon name
					DrawString(weaponName, EnemyXY[0] - tSize.left + width / 2, EnemyXY[1] - tSize.top, 255, 255, 255, pFontSmall);
				}

				//Draw Player Name
				DrawString(const_cast<LPSTR>(PlayerList[i - 1].Name), EnemyXY[0] - tSize.left + width / 2, (EnemyXY[1] - tSize.top) + height - 15, 255, 255, 255, pFontSmall);

				//Draw health int
				std::stringstream strHealth;
				strHealth << PlayerList[i - 1].Health;
				DrawString(strHealth.str().c_str(), (EnemyXY[0] - tSize.left + width / 2) - 25, HeadXY[1] - tSize.top, 255, 255, 255, pFontSmall);

				//Draw armor int
				std::stringstream strArmor;
				strArmor << PlayerList[i - 1].Armor;
				DrawString(strArmor.str().c_str(), (EnemyXY[0] - tSize.left - width / 2) + 10, HeadXY[1] - tSize.top, 255, 255, 255, pFontSmall);

				//Draw distance
				std::stringstream strDistance;
				strDistance << (int)(distance / 40);
				DrawString((strDistance.str() + "m").c_str(), EnemyXY[0] - tSize.left + width / 2, (EnemyXY[1] - tSize.top) + 10, 255, 255, 255, pFontSmall);

				//Entity distance line
				DrawLine((tSize.right - tSize.left) / 2, tSize.bottom - tSize.top, EnemyXY[0] - tSize.left, EnemyXY[1] - tSize.top, r, g, b, 255);
			}
		}
	}
}