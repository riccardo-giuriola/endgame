#include "../Include/hMath.h"

namespace endgame
{
	namespace math
	{
		float Get3dDistance(float * myCoords, float * enemyCoords)
		{
			return sqrt(
				pow(double(enemyCoords[0] - myCoords[0]), 2.0) +
				pow(double(enemyCoords[1] - myCoords[1]), 2.0) +
				pow(double(enemyCoords[2] - myCoords[2]), 2.0));

		}

		bool WorldToScreen(float * from, float * to, float flMatrix[4][4])
		{
			float w = 0.0f;

			to[0] = flMatrix[0][0] * from[0] + flMatrix[0][1] * from[1] + flMatrix[0][2] * from[2] + flMatrix[0][3];
			to[1] = flMatrix[1][0] * from[0] + flMatrix[1][1] * from[1] + flMatrix[1][2] * from[2] + flMatrix[1][3];
			w = flMatrix[3][0] * from[0] + flMatrix[3][1] * from[1] + flMatrix[3][2] * from[2] + flMatrix[3][3];

			if (w < 0.01f)
				return false;

			float invw = 1.0f / w;
			to[0] *= invw;
			to[1] *= invw;

			width = (int)(tSize.right - tSize.left);
			height = (int)(tSize.bottom - tSize.top);

			float x = width / 2;
			float y = height / 2;

			x += 0.5 * to[0] * width + 0.5;
			y -= 0.5 * to[1] * height + 0.5;

			to[0] = x + tSize.left;
			to[1] = y + tSize.top;

			return true;
		}
	}
}