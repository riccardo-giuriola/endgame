#include <Windows.h>
#include <iostream>
#include "../Include/bhop.h"
#include "../Include/MemoryHandling.h"
#include "../Include/Offsets.h"

struct Bhop
{
	int flag;
	
	void ReadInfo()
	{
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwLocalPlayer), &endgame::offsets::localPlayer, sizeof(DWORD), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::localPlayer + hazedumper::netvars::m_fFlags), &flag, sizeof(int), 0);
	}

}bhopData;

void endgame::bhop::Run()
{                 
	std::cout << "Bhop running" << std::endl;
	while (true)
	{
		bhopData.ReadInfo();
		
		if ((GetAsyncKeyState(VK_SPACE) & 0x8000) && bhopData.flag == 257)
		{
			SendMessage(endgame::offsets::windowHandle, WM_KEYDOWN, VK_SPACE, 0x390000);
			SendMessage(endgame::offsets::windowHandle, WM_KEYUP, VK_SPACE, 0x390000);
		}

		Sleep(1);
	}
}
