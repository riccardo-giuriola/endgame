#include "../Include/CAuth.h"
#include "../Include/CDB_Connect.h"

std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

bool CAuth::Login(std::string username, std::string password)
{
	auto user_data = CDB_Connect::Query(("SELECT username, password FROM users WHERE username = '" + username + "' AND password = sha1('" + password + "')").c_str());

	if (!user_data[0].empty())
	{
		std::wstring local_hwid = machine_guid(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\Cryptography", L"MachineGuid");
		auto machine_guid = CDB_Connect::Query(("SELECT machine_guid FROM users WHERE username = '" + username + "'").c_str());

		if (!machine_guid[0].empty())
		{

			if (machine_guid[0][0] != NULL)
			{
				std::wstring stored_hwid = converter.from_bytes(machine_guid[0][0]);
				std::string str_stored_hwid = converter.to_bytes(stored_hwid);
				std::string str_local_hwid = converter.to_bytes(local_hwid);
				std::cout << str_stored_hwid << std::endl;
				std::cout << local_hwid.c_str() << std::endl;

				if (local_hwid == stored_hwid)
				{
					std::cout << "\nWelcome " + username + ", you are now logged in." << std::endl;
					std::wcout << "Your Hardware ID is: " << stored_hwid << std::endl;

					return true;
				}
				else
				{
					std::cout << "This machine is not authorized to play on this account." << std::endl;

					return false;
				}
			}
			else
			{
				std::string str_local_hwid = converter.to_bytes(local_hwid);
				CDB_Connect::Query(("UPDATE users SET machine_guid = '" + str_local_hwid + "' WHERE username = 'pastahead'").c_str());
				std::cout << "MachineGuid updated successfully." << std::endl;

				return true;
			}
		}
		return false;
	}
	else
	{
		std::cout << "\nWrong credentials, try again.\n";
		return false;
	}
}


std::wstring CAuth::machine_guid(HKEY root, std::wstring key, std::wstring name)
{
	HKEY hKey;
	if (RegOpenKeyEx(root, key.c_str(), 0, KEY_READ | KEY_WOW64_64KEY, &hKey) != ERROR_SUCCESS)
		throw "Could not open registry key";

	DWORD type;
	DWORD cbData;
	if (RegQueryValueEx(hKey, name.c_str(), NULL, &type, NULL, &cbData) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		throw "Could not read registry value";
	}

	if (type != REG_SZ)
	{
		RegCloseKey(hKey);
		throw "Incorrect registry value type";
	}

	std::wstring value(cbData / sizeof(wchar_t), L'\0');
	if (RegQueryValueEx(hKey, name.c_str(), NULL, NULL, reinterpret_cast<LPBYTE>(&value[0]), &cbData) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		throw "Could not read registry value";
	}
	RegCloseKey(hKey);

	size_t firstNull = value.find_first_of(L'\0');
	if (firstNull != std::string::npos)
		value.resize(firstNull);

	return value;
}
