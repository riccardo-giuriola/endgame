#include <iostream>
#include <Windows.h>
#include "../Include/MemoryHandling.h"
#include "../Include/Offsets.h"
#include <tlhelp32.h>
#include <wchar.h>

namespace endgame
{
	namespace offsets
	{
		DWORD pId                      = 0;
		DWORD clientPanorama           = 0x0;
		DWORD engineModule             = 0x0;
		DWORD localPlayer              = 0x0;

		HANDLE hProc                   = INVALID_HANDLE_VALUE;
		HWND windowHandle			   = NULL;
	}
}

DWORD endgame::memoryhandling::GetModBaseAddr(const wchar_t* moduleName)
{
	if (endgame::offsets::windowHandle == NULL)
	{
		while (endgame::offsets::windowHandle == NULL)
		{
			std::cout << "Waiting for csgo..." << '\r';
			endgame::offsets::windowHandle = FindWindow(0, L"Counter-Strike: Global Offensive");
			Sleep(1000);
		}
	}

	GetWindowThreadProcessId(endgame::offsets::windowHandle, &endgame::offsets::pId);
	
	MODULEENTRY32 me;
	me.dwSize = sizeof(MODULEENTRY32);

	if (endgame::offsets::pId)
	{
		if (HANDLE mHandle = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE32 | TH32CS_SNAPMODULE, offsets::pId))
		{
			if (!Module32First(mHandle, &me))
			{
				std::cerr << "M32F() error" << std::endl;
				CloseHandle(mHandle);

				return 0;
			}
			
			do
			{
				/*for (const char i : me.szModule)
					std::cout << i;

				std::cout << std::endl;*/
				const size_t cSize = strlen(_bstr_t(me.szModule)) + 1;
				wchar_t* wc = new wchar_t[cSize];
				mbstowcs(wc, _bstr_t(me.szModule), cSize);
				if (wcscmp(wc, moduleName) == 0)
				{
					CloseHandle(mHandle);

					return (DWORD)me.modBaseAddr;
				}
			} while (Module32Next(mHandle, &me));
		}
		else
		{
			std::cerr << "CTH32S() error" << std::endl;
			
			return 0;
		}
	}
	else
	{
		std::cerr << "pId error" << std::endl;

		return 0;
	}
}

void endgame::memoryreading::GetLocalPlayer()
{
}