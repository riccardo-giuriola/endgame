#include "../Include/CDB_Connect.h"

const char* CDB_Connect::ip      = "localhost";
const char* CDB_Connect::user    = "root";
const char* CDB_Connect::passwd  = "password";
const char* CDB_Connect::db_name = "endgame";

std::vector<std::vector<char*>> CDB_Connect::Query(const char* query)
{
	std::vector<std::vector<char*>> result(1);
	MYSQL* conn;
	MYSQL_ROW row;
	MYSQL_RES* res;

	conn = mysql_init(0);
	conn = mysql_real_connect(conn, ip, user, passwd, db_name, port, NULL, 0);

	if (conn)
	{
		int qstate = mysql_query(conn, query);

		if (!qstate)
		{
			res = mysql_store_result(conn);

			if (res != NULL)
			{
				unsigned i = 0;

				while (row = mysql_fetch_row(res))
				{
					for (unsigned j = 0; j < sizeof(row); j++)
						result[i].push_back(row[j]);

					++i;
					result.resize(result.size()+1);
				}
			}

			return result;
		}
		else
			std::cout << mysql_error(conn) << std::endl;

	}
	else
		std::cout << "Can't contact the server, please check your connection and retry." << std::endl;

}