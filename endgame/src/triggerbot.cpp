#include <iostream>
#include <Windows.h>
#include "../Include/triggerbot.h"
#include "../Include/MemoryHandling.h"
#include "../Include/Offsets.h"

struct Triggerbot
{
	int entityInCrosshairID;
	int currentTeam;
	int enemyTeam;
	int enemyHealth;
	DWORD entityInCrosshairAddress;

	void ReadMemory()
	{
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwLocalPlayer), &endgame::offsets::localPlayer, sizeof(DWORD), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::localPlayer + hazedumper::netvars::m_iCrosshairId), &entityInCrosshairID, sizeof(int), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::localPlayer + hazedumper::netvars::m_iTeamNum), &currentTeam, sizeof(int), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(endgame::offsets::clientPanorama + hazedumper::signatures::dwEntityList + ((entityInCrosshairID - 1) * 0x10)), &entityInCrosshairAddress, sizeof(DWORD), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(entityInCrosshairAddress + hazedumper::netvars::m_iTeamNum), &enemyTeam, sizeof(int), 0);
		ReadProcessMemory(endgame::offsets::hProc, (PBYTE*)(entityInCrosshairAddress + hazedumper::netvars::m_iHealth), &enemyHealth, sizeof(int), 0);
	}
}triggerbotData;

void endgame::triggerbot::Run()
{
	std::cout << "Triggerbot running" << std::endl;
	while (true)
	{
		triggerbotData.ReadMemory();

		if (triggerbotData.currentTeam != triggerbotData.enemyTeam && triggerbotData.enemyHealth > 0 && triggerbotData.entityInCrosshairID > 0 && triggerbotData.entityInCrosshairID < 32)
		{
			//Sleep(50);
			mouse_event(MOUSEEVENTF_LEFTDOWN, NULL, NULL, NULL, NULL);
			Sleep(10);
			mouse_event(MOUSEEVENTF_LEFTUP, NULL, NULL, NULL, NULL);
		}
		Sleep(1);
	}
}