//EndGame CS:GO Hacks
//Creation date 02/05/2019

#include <iostream>
#include <Windows.h>
#include <fstream>
#include <string>
#include <thread>
#include <stdio.h>
#include <urlmon.h>
#include <direct.h>
#include <conio.h>
#include "../Include/Offsets.h"
#include "../Include/CAuth.h"
#include "../Include/MemoryHandling.h"
#include "../Include/triggerbot.h"
#include "../Include/bhop.h"
#include "../Include/aimbot.h"
#include "../include/hMain.h"

int width  = NULL;
int height = NULL;

const MARGINS Margin = { 0, 0, width, height };

LPCWSTR lWindowName = L"Endgame Private Cheat";
LPCWSTR tWindowName = L"Counter-Strike: Global Offensive";
HWND hWnd;
RECT tSize;
MSG Message;

bool esp = false;

void readFile(const char * varName, int& offsetName )
{
	std::fstream inFile;
	inFile.open("offsets.txt");
	std::string tmpLine;

	while (std::getline(inFile, tmpLine))
	{
		if (strstr(tmpLine.c_str(), varName))
		{
			unsigned i = 0;

			for (; i < tmpLine.length(); ++i)
			{
				if (tmpLine.at(i) == '=')
				{
					++i;

					break;
				}
			}

			std::string value("");

			for (unsigned z = i + 1; z < tmpLine.length() - 1; ++z)
				value += tmpLine.at(z);

			//std::cout << value << std::endl;
			std::cout << "Updating address " << varName << std::endl;
			offsetName = std::strtol(value.c_str(), NULL, 0);
			//std::cout << std::hex << offsetName << std::endl;

			break;
		}
	}
	Sleep(50);
}

bool fexists(const char *filename) {
	std::ifstream ifile(filename);
	return (bool)ifile;
}

bool loadOffsets()
{
	char workingDirectory[FILENAME_MAX];
	_getcwd(workingDirectory, FILENAME_MAX);
	std::string strWorkingDirectory(workingDirectory);

	auto AddQuotations = [&strWorkingDirectory]() -> std::string
	{
		std::string toRet("\"\"");

		for (unsigned i = 0; i < strWorkingDirectory.length(); ++i)
			toRet += strWorkingDirectory.at(i);

		return toRet += "\"\"";
	};

	std::string temp = AddQuotations();
	std::cout << temp << std::endl;
	system((temp + "\"\"/download_offsets.exe\"\"").c_str());

	while (true)
	{
		Sleep(100);
		if (fexists("offsets.txt"))
			break;
	}
	
	std::cout << "SEARCHING FOR UPDATES..." << std::endl;
	std::cout << std::endl;
	Sleep(500);

	//netvars
	readFile("m_ArmorValue", hazedumper::netvars::m_ArmorValue);
	readFile("m_aimPunchAngle", hazedumper::netvars::m_aimPunchAngle);
	readFile("m_aimPunchAngleVel", hazedumper::netvars::m_aimPunchAngleVel);
	readFile("m_bBombPlanted", hazedumper::netvars::m_bBombPlanted);
	readFile("m_bHasDefuser", hazedumper::netvars::m_bHasDefuser);
	readFile("m_bHasHelmet", hazedumper::netvars::m_bHasHelmet);
	readFile("m_bIsDefusing", hazedumper::netvars::m_bIsDefusing);
	readFile("m_bIsScoped", hazedumper::netvars::m_bIsScoped);
	readFile("m_bSpotted", hazedumper::netvars::m_bSpotted);
	readFile("m_bSpottedByMask", hazedumper::netvars::m_bSpottedByMask);
	readFile("m_dwBoneMatrix", hazedumper::netvars::m_dwBoneMatrix);
	readFile("m_fFlags", hazedumper::netvars::m_fFlags);
	readFile("m_flC4Blow", hazedumper::netvars::m_flC4Blow);
	readFile("m_flDefuseCountDown", hazedumper::netvars::m_flDefuseCountDown);
	readFile("m_flDefuseLength", hazedumper::netvars::m_flDefuseLength);
	readFile("m_hActiveWeapon", hazedumper::netvars::m_hActiveWeapon);
	readFile("m_iCrosshairId", hazedumper::netvars::m_iCrosshairId);
	readFile("m_iFOV", hazedumper::netvars::m_iFOV);
	readFile("m_iHealth", hazedumper::netvars::m_iHealth);
	readFile("m_iItemDefinitionIndex", hazedumper::netvars::m_iItemDefinitionIndex);
	readFile("m_iItemIDHigh", hazedumper::netvars::m_iItemIDHigh);
	readFile("m_iShotsFired", hazedumper::netvars::m_iShotsFired);
	readFile("m_iState", hazedumper::netvars::m_iState);
	readFile("m_iTeamNum", hazedumper::netvars::m_iTeamNum);
	readFile("m_lifeState", hazedumper::netvars::m_lifeState);
	readFile("m_vecOrigin", hazedumper::netvars::m_vecOrigin);
	readFile("m_vecViewOffset", hazedumper::netvars::m_vecViewOffset);

	//signatures
	readFile("dwClientState", hazedumper::signatures::dwClientState);
	readFile("dwClientState_MaxPlayer", hazedumper::signatures::dwClientState_MaxPlayer);
	readFile("dwClientState_PlayerInfo", hazedumper::signatures::dwClientState_PlayerInfo);
	readFile("dwClientState_ViewAngles", hazedumper::signatures::dwClientState_ViewAngles);
	readFile("dwEntityList", hazedumper::signatures::dwEntityList);
	readFile("dwGameRulesProxy", hazedumper::signatures::dwGameRulesProxy);
	readFile("dwLocalPlayer", hazedumper::signatures::dwLocalPlayer);
	readFile("dwRadarBase", hazedumper::signatures::dwRadarBase); 
	readFile("dwViewMatrix", hazedumper::signatures::dwViewMatrix);
	readFile("dwWeaponTableIndex", hazedumper::signatures::dwWeaponTableIndex);
	readFile("m_bDormant ", hazedumper::signatures::m_bDormant);
	
	remove("offsets.txt");

	return true;
}

LRESULT CALLBACK WinProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	switch (Message)
	{
	case WM_PAINT:
		Render();
		break;

	case WM_CREATE:
		DwmExtendFrameIntoClientArea(hWnd, &Margin);
		break;

	case WM_DESTROY:
		PostQuitMessage(1);
		return 0;

	default:
		return DefWindowProc(hWnd, Message, wParam, lParam);
		break;
	}
	return 0;
}

void SetWindowToTarget()
{
	while (true)
	{
		endgame::offsets::windowHandle = FindWindow(0, tWindowName);
		if (endgame::offsets::windowHandle)
		{
			GetWindowRect(endgame::offsets::windowHandle, &tSize);
			width = tSize.right - tSize.left;
			height = tSize.bottom - tSize.top;
			/*std::cout << width << " | " << height << std::endl;*/
			DWORD dwStyle = GetWindowLong(endgame::offsets::windowHandle, GWL_STYLE);
			if (dwStyle & WS_BORDER)
			{
				tSize.top += 23;
				height -= 23;
			}
			MoveWindow(hWnd, tSize.left, tSize.top, width, height, true);
		}
		Sleep(1000);
	}
}

void keyListener()
{
	HANDLE tbThread = 0;
	HANDLE bhThread = 0;
	HANDLE abThread = 0;
	HANDLE rcsThread = 0;

	while (true)
	{
		if (GetAsyncKeyState(VK_NUMPAD1))
		{
			if (tbThread == 0)
			{
				tbThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)endgame::triggerbot::Run, 0, 0, 0);
			}
			else if (tbThread != 0)
			{
				TerminateThread(tbThread, 0);
				tbThread = 0;
				std::cout << "Triggerbot stopped" << std::endl;
			}
			Sleep(500);
		}
		if (GetAsyncKeyState(VK_NUMPAD2))
		{
			if (bhThread == 0)
			{
				bhThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)endgame::bhop::Run, 0, 0, 0);
			}
			else if (bhThread != 0)
			{
				TerminateThread(bhThread, 0);
				bhThread = 0;
				std::cout << "Bhop stopped" << std::endl;
			}
			Sleep(500);
		}
		if (GetAsyncKeyState(VK_NUMPAD3))
		{
			if (abThread == 0)
			{
				abThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)endgame::aimbot::Run, 0, 0, 0);
			}
			else if (abThread != 0)
			{
				TerminateThread(abThread, 0);
				abThread = 0;
				std::cout << "Aimbot stopped" << std::endl;
			}
			Sleep(500);
		}

		if (GetAsyncKeyState(VK_NUMPAD4))
		{
			if (rcsThread == 0)
			{
				rcsThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)endgame::aimbot::RecoilControl, 0, 0, 0);
			}
			else if (rcsThread != 0)
			{
				TerminateThread(rcsThread, 0);
				rcsThread = 0;
				std::cout << "RCS stopped" << std::endl;
			}
			Sleep(500);
		}
		if (GetAsyncKeyState(VK_NUMPAD5))
		{
			esp = !esp;
			if (!esp)
			{
				p_Device->EndScene();
				p_Device->PresentEx(0, 0, 0, 0, 0);
				p_Device->Clear(0, 0, D3DCLEAR_TARGET, 0, 1.0f, 0);
				Sleep(1000);
			}
			Sleep(500);
		}
		if (GetAsyncKeyState(VK_INSERT))
		{
			CloseHandle(endgame::offsets::hProc);
			exit(1);
		}

		Sleep(10);
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	RECT rect;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &rect);
	int consoleWidth = rect.right - rect.left;
	int consoleHeight = rect.bottom - rect.top;
	
	AllocConsole();
	freopen("conin$", "r", stdin);
	freopen("conout$", "w", stdout);
	freopen("conout$", "w", stderr);

	system("color 0A");

	SetWindowPos(GetConsoleWindow(), NULL, (consoleWidth / 2) - 400, (consoleHeight / 2) - 200, 800, 400, SWP_SHOWWINDOW);

	std::string username("");
	std::string password("");

	std::cout << R"(
 ______ _   _ _____   _____          __  __ ______    _____ _    _ ______       _______ _____ 
|  ____| \ | |  __ \ / ____|   /\   |  \/  |  ____|  / ____| |  | |  ____|   /\|__   __/ ____|
| |__  |  \| | |  | | |  __   /  \  | \  / | |__    | |    | |__| | |__     /  \  | | | (___  
|  __| | . ` | |  | | | |_ | / /\ \ | |\/| |  __|   | |    |  __  |  __|   / /\ \ | |  \___ \ 
| |____| |\  | |__| | |__| |/ ____ \| |  | | |____  | |____| |  | | |____ / ____ \| |  ____) |
|______|_| \_|_____/ \_____/_/    \_\_|  |_|______|  \_____|_|  |_|______/_/    \_\_| |_____/ 
		
	)" << std::endl;

	std::cout << "EndGame CS:GO Hacks	";
	std::cout << "Version 0.1	";
	std::cout << "www.endgamecheats.com\n" << std::endl;

	std::cout << "LOGIN" << std::endl;

	do
	{
		std::cout << "Username: ";
		std::cin >> username;
		std::cout << "Password: ";
		std::cin >> password;
		std::cout << std::endl;
	} while (!CAuth::Login(username, password));

	if (loadOffsets())
	{
		std::cout << "Update completed." << std::endl;
		std::cout << std::endl;
	}

	endgame::offsets::clientPanorama = endgame::memoryhandling::GetModBaseAddr(L"client_panorama.dll");
	std::cout << endgame::offsets::clientPanorama;
	endgame::offsets::engineModule   = endgame::memoryhandling::GetModBaseAddr(L"engine.dll");
	std::cout << endgame::offsets::engineModule;
	endgame::offsets::hProc          = OpenProcess(PROCESS_VM_READ/*PROCESS_ALL_ACCESS*/, FALSE, endgame::offsets::pId);

	ShowWindow(::GetConsoleWindow(), SW_HIDE);

	CreateThread(0, 0, (LPTHREAD_START_ROUTINE)keyListener, 0, 0, 0);

	CreateThread(0, 0, (LPTHREAD_START_ROUTINE)SetWindowToTarget, 0, 0, 0);

	WNDCLASSEX wClass;
	wClass.cbClsExtra = NULL;
	wClass.cbSize = sizeof(WNDCLASSEX);
	wClass.cbWndExtra = NULL;
	wClass.hbrBackground = (HBRUSH)CreateSolidBrush(RGB(0, 0, 0));
	wClass.hCursor = LoadCursor(0, IDC_ARROW);
	wClass.hIcon = LoadIcon(0, IDI_APPLICATION);
	wClass.hIconSm = LoadIcon(0, IDI_APPLICATION);
	wClass.hInstance = hInstance;
	wClass.lpfnWndProc = WinProc;
	wClass.lpszClassName = lWindowName;
	wClass.lpszMenuName = lWindowName;
	wClass.style = CS_VREDRAW | CS_HREDRAW;

	if (!RegisterClassEx(&wClass))
		exit(1);

	if (endgame::offsets::windowHandle)
	{
		hWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TRANSPARENT | WS_EX_LAYERED, lWindowName, lWindowName, WS_POPUP, 1, 1, width, height, 0, 0, 0, 0);
		SetLayeredWindowAttributes(hWnd, 0, 1.0f, LWA_ALPHA);
		SetLayeredWindowAttributes(hWnd, 0, RGB(0, 0, 0), LWA_COLORKEY);
		ShowWindow(hWnd, SW_SHOW);
	}

	DirectXInit(hWnd);

	for (;;)
	{
		if (PeekMessage(&Message, hWnd, 0, 0, PM_REMOVE))
		{
			DispatchMessage(&Message);
			TranslateMessage(&Message);
		}
	}

	system("pause");

	return 0;
}