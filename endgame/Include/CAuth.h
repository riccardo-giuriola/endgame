#pragma once
#include <codecvt>
#include <string>
#include "CDB_Connect.h"

class CAuth
{
public:
	static bool Login(std::string username, std::string password);
	static std::wstring machine_guid(HKEY root, std::wstring key, std::wstring name);

};