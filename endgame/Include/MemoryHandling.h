#pragma once

#include <Windows.h>
#include <comdef.h>

namespace endgame
{
	namespace memoryhandling
	{
		DWORD GetModBaseAddr(const wchar_t* moduleName);
	}

	namespace memoryreading
	{
		void GetLocalPlayer();
	}

	namespace offsets
	{

		extern DWORD pId;
		extern DWORD clientPanorama;
		extern DWORD engineModule;
		extern DWORD localPlayer;

		extern HANDLE hProc;
		extern HWND windowHandle;
	}
}