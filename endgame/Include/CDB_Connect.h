#pragma once
#include <mysql.h>
#include <vector>
#include <iostream>
#include <string>

class CDB_Connect
{
private:
	static const char* ip;
	static const char* user;
	static const char* passwd;
	static const char* db_name;

	static constexpr unsigned port = 3306;

public:
	static std::vector<std::vector<char*>> Query(const char* Query);

};