#pragma once
#include <cstdint>

namespace hazedumper
{
	namespace netvars
	{
		extern int m_ArmorValue;
		extern int m_aimPunchAngle;
		extern int m_aimPunchAngleVel;
		extern int m_bBombPlanted;
		extern int m_bHasDefuser;
		extern int m_bHasHelmet;
		extern int m_bIsDefusing;
		extern int m_bIsScoped;
		extern int m_bSpotted;
		extern int m_bSpottedByMask;
		extern int m_dwBoneMatrix;
		extern int m_fFlags;
		extern int m_flC4Blow;
		extern int m_flDefuseCountDown;
		extern int m_flDefuseLength;
		extern int m_hActiveWeapon;
		extern int m_iCrosshairId;
		extern int m_iFOV;
		extern int m_iHealth;
		extern int m_iItemDefinitionIndex;
		extern int m_iItemIDHigh;
		extern int m_iShotsFired;
		extern int m_iState;
		extern int m_iTeamNum;
		extern int m_lifeState;
		extern int m_vecOrigin;
		extern int m_vecViewOffset;
	}

	namespace signatures
	{
		extern int dwClientState;
		extern int dwClientState_MaxPlayer;
		extern int dwClientState_PlayerInfo;
		extern int dwClientState_ViewAngles;
		extern int dwEntityList;
		extern int dwGameRulesProxy;
		extern int dwLocalPlayer;
		extern int dwRadarBase;
		extern int dwViewMatrix;
		extern int dwWeaponTableIndex;
		extern int m_bDormant;
	}
}