#pragma once
#include "hMain.h"

namespace endgame
{
	namespace math
	{
		float Get3dDistance(float * myCoords, float * enemyCoords);
		bool WorldToScreen(float * from, float * to, float flMatrix[4][4]);
	}
}