#pragma once

#include "hDirectX.h"

#include <Windows.h>
#include <iostream>
#include <algorithm>
#include <shlwapi.h>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>

#include <d3d9.h>
#include <d3dx9.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#include <dwmapi.h>
#pragma comment(lib, "dwmapi.lib")

extern int width;
extern int height;
extern LPCWSTR lWindowName;
extern HWND hWnd;
extern LPCWSTR tWindowName;
extern RECT tSize;
extern MSG Message;
extern bool Debug_Border;
extern bool esp;

void readFile(const char * varName, int& offsetName);
bool fexists(const char *filename);
bool loadOffsets();
LRESULT CALLBACK WinProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam);
void SetWindowToTarget();
void KeyListener();
//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hSecInstance, LPSTR nCmdLine, INT nCmdShow);